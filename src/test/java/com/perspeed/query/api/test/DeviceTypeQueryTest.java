package com.perspeed.query.api.test;

import java.util.Arrays;

import org.joda.time.DateTime;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.perfspeed.query.service.fetcher.DeviceTypeFetcher;
import com.perfspeed.query.service.resp.chart.PerfPieChatResponse;
import com.perfspeed.query.service.utils.JsonParser;

import in.zapr.druid.druidry.Interval;
import in.zapr.druid.druidry.aggregator.CountAggregator;
import in.zapr.druid.druidry.aggregator.DruidAggregator;
import in.zapr.druid.druidry.client.DruidClient;
import in.zapr.druid.druidry.client.DruidConfiguration;
import in.zapr.druid.druidry.client.DruidJerseyClient;
import in.zapr.druid.druidry.dimension.DefaultDimension;
import in.zapr.druid.druidry.dimension.DruidDimension;
import in.zapr.druid.druidry.dimension.enums.OutputType;
import in.zapr.druid.druidry.granularity.Granularity;
import in.zapr.druid.druidry.granularity.PredefinedGranularity;
import in.zapr.druid.druidry.granularity.SimpleGranularity;
import in.zapr.druid.druidry.query.aggregation.DruidTopNQuery;
import in.zapr.druid.druidry.topNMetric.SimpleMetric;
import in.zapr.druid.druidry.topNMetric.TopNMetric;

public class DeviceTypeQueryTest {

	public static void main(String[] args) throws Exception {
		final DruidAggregator deviceTypeCount = new CountAggregator("DeviceTypeCount");
		final DruidDimension dimension = new DefaultDimension("deviceType", "DeviceType", OutputType.STRING);
		final Granularity granularity = new SimpleGranularity(PredefinedGranularity.ALL);
		final DateTime endTime = new DateTime().plusDays(365);
		final DateTime startTime = new DateTime().minusDays(10);
		final Interval interval = new Interval(startTime, endTime);
		
		final TopNMetric metric = new SimpleMetric("DeviceTypeCount");
		final DruidTopNQuery topNQuery = DruidTopNQuery.builder()
										 .aggregators(Arrays.asList(deviceTypeCount))
										 .dataSource("beacons")
										 .dimension(dimension)
										 .granularity(granularity)
										 .intervals(Arrays.asList(interval))
										 .topNMetric(metric)
										 .threshold(5000)
										 .build();
		
		ObjectMapper jsonParser = new ObjectMapper();
		System.out.println(jsonParser.writeValueAsString(topNQuery));
		final DruidConfiguration config =  DruidConfiguration
	               .builder()
	               .host("ec2-52-66-183-158.ap-south-1.compute.amazonaws.com")
	               .endpoint("druid/v2/")
	               .build();
		final DruidClient client = new DruidJerseyClient(config);
		client.connect();
		final String resp = client.query(topNQuery);
		System.out.println(resp);
		
		final PerfPieChatResponse pieChartResp = new PerfPieChatResponse();
		DeviceTypeFetcher fetcher = new DeviceTypeFetcher("");
		fetcher.parseResponse(resp, pieChartResp);
		System.out.println(JsonParser.objectToJson(pieChartResp));
	}
}
