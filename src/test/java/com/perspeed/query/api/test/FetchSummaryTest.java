package com.perspeed.query.api.test;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

import org.joda.time.DateTime;

import com.perfspeed.query.service.entity.QueryParams;
import com.perfspeed.query.service.fetcher.SummaryMetricsFetcher;
import com.perfspeed.query.service.querybuilder.SummaryQueryBuilder;
import com.perfspeed.query.service.resp.Response;
import com.perfspeed.query.service.resp.SummaryMetrics;
import com.perfspeed.query.service.utils.DruidUtils;
import com.perfspeed.query.service.utils.JsonParser;

import in.zapr.druid.druidry.client.DruidClient;
import in.zapr.druid.druidry.client.DruidConfiguration;
import in.zapr.druid.druidry.client.DruidJerseyClient;
import in.zapr.druid.druidry.query.aggregation.DruidTimeSeriesQuery;

public class FetchSummaryTest {

	public static void main(String[] args) throws Exception {
		final DateTime endTime = DateTime.now();
		final DateTime startTime = DateTime.now().minusDays(1);
		final QueryParams query = new QueryParams();
		query.setFromDate(startTime.getMillis());
		query.setToDate(endTime.getMillis());
		final List<Response> result = new ArrayList<>();
		final DruidTimeSeriesQuery timeSeriesQuery = SummaryQueryBuilder.getSummarySQL("beacons", query);
		SummaryMetricsFetcher fetcher = new SummaryMetricsFetcher("beacons");
		final DruidConfiguration config =  DruidConfiguration
	               .builder()
	               .host("ec2-52-66-183-158.ap-south-1.compute.amazonaws.com")
	               .endpoint("druid/v2/")
	               .build();
		final DruidClient client = new DruidJerseyClient(config);
		client.connect();
		final String resp = client.query(timeSeriesQuery);
		fetcher.parseSummaryResponse(resp, result);
		
		final DruidTimeSeriesQuery timeSeriesQuerySession = SummaryQueryBuilder.getSessionTimeSQL("beacons", query);
		System.out.println(JsonParser.objectToJson(timeSeriesQuery));
		final String respSession = client.query(timeSeriesQuerySession);
		System.out.println("Resp : "+respSession);
		if(Objects.isNull(respSession) || respSession.isEmpty() ) return ;
		final SummaryMetrics metrics = JsonParser.jsonToObject(DruidUtils.extractResultFromResp(respSession), SummaryMetrics.class);
		final Response sessionTime = new Response();
		sessionTime.setName("Session Time");
		sessionTime.setValue(metrics.getSessionTime());
		result.add(sessionTime);
		/*
		final DruidTimeSeriesQuery timeSeriesQuerySessionCompletion = SummaryQueryBuilder.getCompletedSessionSQL("beacons", query);
		System.out.println(JsonParser.objectToJson(timeSeriesQuery));
		final String respSessionComp = client.query(timeSeriesQuerySessionCompletion);
		System.out.println("Resp : "+respSessionComp);
		if(Objects.isNull(respSessionComp) || respSessionComp.isEmpty() ) return ;
		final SummaryMetrics metricsCompletion = JsonParser.jsonToObject(DruidUtils.extractResultFromResp(respSessionComp), SummaryMetrics.class);
		final Response sessionTotal = new Response();
		sessionTotal.setName("NoOfCompletedSessions");
		sessionTotal.setValue(metricsCompletion.getCompletedSessions());
		result.add(sessionTotal);
		*/
		System.out.println(JsonParser.objectToJson(result));
		
	}
}