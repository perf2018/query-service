package com.perspeed.query.api.test;

import java.util.List;

import org.joda.time.DateTime;

import com.perfspeed.query.service.entity.QueryParams;
import com.perfspeed.query.service.fetcher.PerfHistoMetircsFetcher;
import com.perfspeed.query.service.querybuilder.PerfHistoQueryBuilder;
import com.perfspeed.query.service.resp.chart.PerfHistoChatResponse;
import com.perfspeed.query.service.resp.druid.DruidQueryHistoResp;
import com.perfspeed.query.service.utils.JsonParser;

import in.zapr.druid.druidry.Interval;
import in.zapr.druid.druidry.client.DruidClient;
import in.zapr.druid.druidry.client.DruidConfiguration;
import in.zapr.druid.druidry.client.DruidJerseyClient;
import in.zapr.druid.druidry.query.aggregation.DruidTimeSeriesQuery;

public class PerfHistoMetricsTest {
	
	public static void main(String[] args) throws Exception {
		final DateTime endTime = DateTime.now().minusHours(2);
		final DateTime startTime = DateTime.now().minusHours(5);
		final Interval interval = new Interval(startTime, endTime);
		System.out.println("interval : "+interval.getEndTime() +" "+interval.getStartTime());
		QueryParams params = new QueryParams();
		params.setFromDate(startTime.getMillis());
		params.setToDate(endTime.getMillis());
		final DruidTimeSeriesQuery timeSeriesQuery = PerfHistoQueryBuilder.getPerfTimeSQL("beacons", params);
		System.out.println(JsonParser.objectToJson(timeSeriesQuery));
		final DruidConfiguration config =  DruidConfiguration
	               .builder()
	               .host("ec2-52-66-183-158.ap-south-1.compute.amazonaws.com")
	               .endpoint("druid/v2/")
	               .build();
		final DruidClient client = new DruidJerseyClient(config);
		client.connect();
		final String resp = client.query(timeSeriesQuery);
		System.out.println("Resp ==== >  "+resp);
		List<DruidQueryHistoResp> histoResp = JsonParser.json2ObjList(resp);
		System.out.println(histoResp);
		System.out.println(resp);
		
		PerfHistoMetircsFetcher fetcher = new PerfHistoMetircsFetcher("beacons");
		final PerfHistoChatResponse histogramResp = new PerfHistoChatResponse();
		fetcher.parseResp(histogramResp, resp);
		System.out.println(histogramResp);
		System.out.println(JsonParser.objectToJson(histogramResp));
	}
}
