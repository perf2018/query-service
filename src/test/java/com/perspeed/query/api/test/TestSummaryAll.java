package com.perspeed.query.api.test;

import java.util.Arrays;

import org.joda.time.DateTime;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;

import in.zapr.druid.druidry.Interval;
import in.zapr.druid.druidry.aggregator.CountAggregator;
import in.zapr.druid.druidry.aggregator.DruidAggregator;
import in.zapr.druid.druidry.aggregator.LongSumAggregator;
import in.zapr.druid.druidry.client.DruidClient;
import in.zapr.druid.druidry.client.DruidConfiguration;
import in.zapr.druid.druidry.client.DruidJerseyClient;
import in.zapr.druid.druidry.client.exception.ConnectionException;
import in.zapr.druid.druidry.client.exception.QueryException;
import in.zapr.druid.druidry.dimension.DefaultDimension;
import in.zapr.druid.druidry.dimension.DruidDimension;
import in.zapr.druid.druidry.dimension.enums.OutputType;
import in.zapr.druid.druidry.granularity.Granularity;
import in.zapr.druid.druidry.granularity.PredefinedGranularity;
import in.zapr.druid.druidry.granularity.SimpleGranularity;
import in.zapr.druid.druidry.postAggregator.ArithmeticFunction;
import in.zapr.druid.druidry.postAggregator.ArithmeticOrdering;
import in.zapr.druid.druidry.postAggregator.ArithmeticPostAggregator;
import in.zapr.druid.druidry.postAggregator.DruidPostAggregator;
import in.zapr.druid.druidry.postAggregator.FieldAccessPostAggregator;
import in.zapr.druid.druidry.query.aggregation.DruidGroupByQuery;

public class TestSummaryAll {

	public static void main(String[] args) throws ConnectionException, QueryException, JsonProcessingException {
		final DruidAggregator pageViews = new CountAggregator("PageViews");
		final DruidAggregator pageLoadSum = new LongSumAggregator("pageload_time:sum", "pageload_time");
		final DruidAggregator pageLoadCount = new CountAggregator("pageload_time:count");
		final DruidPostAggregator pageLoadTimeSum = new FieldAccessPostAggregator("pageload_time:sum");
		final DruidPostAggregator pageLoadTimeCount  = new FieldAccessPostAggregator("pageload_time:count");
		
		final DruidPostAggregator postAggregator = ArithmeticPostAggregator.builder()
											 .name("PageLoadTime")
											 .function(ArithmeticFunction.QUOTIENT)
											 .fields(Arrays.asList(pageLoadTimeSum, pageLoadTimeCount))
											 .ordering(ArithmeticOrdering.NUMERIC_FIRST)
											 .build();
		
		final DateTime endTime = DateTime.now();
		final DateTime startTime = DateTime.now().minusDays(30);
		final Interval interval = new Interval(startTime, endTime);
		System.out.println("interval : "+interval.getEndTime() +" "+interval.getStartTime());
		final Granularity granularity = new SimpleGranularity(PredefinedGranularity.MINUTE);
		final DruidDimension dimension = new DefaultDimension("u", "URL", OutputType.STRING);
		
		final DruidGroupByQuery groupByQuery =  DruidGroupByQuery.builder()
												.dataSource("beacons")
												.dimensions(Arrays.asList(dimension))
												.granularity(granularity)
												.intervals(Arrays.asList(interval))
												.aggregators(Arrays.asList(pageViews,pageLoadSum,pageLoadCount))
												.postAggregators(Arrays.asList(postAggregator))
												.build();
		System.out.println(groupByQuery);
		ObjectMapper jsonParser = new ObjectMapper();
		System.out.println(jsonParser.writeValueAsString(groupByQuery));
		final DruidConfiguration config =  DruidConfiguration
	               .builder()
	               .host("ec2-52-66-183-158.ap-south-1.compute.amazonaws.com")
	               .endpoint("druid/v2/")
	               .build();
		final DruidClient client = new DruidJerseyClient(config);
		client.connect();
		final String resp = client.query(groupByQuery);
		System.out.println(resp);
	}
}
