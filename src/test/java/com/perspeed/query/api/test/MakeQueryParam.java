package com.perspeed.query.api.test;

import org.joda.time.DateTime;

import com.perfspeed.query.service.entity.QueryParams;
import com.perfspeed.query.service.utils.JsonParser;

public class MakeQueryParam {

	public static void main(String[] args) {
		final DateTime endTime = DateTime.now().plusDays(365);
		final DateTime startTime = DateTime.now().minusDays(2);
		QueryParams params = new QueryParams();
		params.setFromDate(startTime.getMillis());
		params.setToDate(endTime.getMillis());
		System.out.println(JsonParser.objectToJson(params));
	}
}