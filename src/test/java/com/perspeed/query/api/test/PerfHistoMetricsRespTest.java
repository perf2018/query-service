package com.perspeed.query.api.test;

import org.joda.time.DateTime;

import com.perfspeed.query.service.entity.QueryParams;
import com.perfspeed.query.service.fetcher.PerfHistoMetircsFetcher;

import in.zapr.druid.druidry.Interval;

public class PerfHistoMetricsRespTest {

	public static void main(String[] args) throws Exception {
		final DateTime endTime = DateTime.now();
		final DateTime startTime = DateTime.now().minusHours(1);
		final Interval interval = new Interval(startTime, endTime);
		System.out.println("interval : "+interval.getEndTime() +" "+interval.getStartTime());
		QueryParams params = new QueryParams();
		params.setFromDate(startTime.getMillis());
		params.setToDate(endTime.getMillis());
		PerfHistoMetircsFetcher fetcher = new PerfHistoMetircsFetcher("beacons");
		System.out.println(fetcher.fetchHistoGramMetrics(params));
	}
}
