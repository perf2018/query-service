package com.perspeed.query.api.test;

import java.io.IOException;

import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.perfspeed.query.service.resp.SummaryMetrics;

public class JsonParserTest {

	public static void main(String[] args) throws JsonParseException, JsonMappingException, IOException {
		String json = "{\"PageLoadTime\":862.4749163879599,\"FrontEndTime\":250.29096989966555,\"PageViews\":299,\"BackEndTime\":564.5986622073578}";
		ObjectMapper jsonParser = new ObjectMapper();
		SummaryMetrics metrics = jsonParser.readValue(json, SummaryMetrics.class);
		System.out.println(metrics);
	}
}
