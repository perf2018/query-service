package com.perspeed.query.api.test;

import org.joda.time.DateTime;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.perfspeed.query.service.entity.QueryParams;
import com.perfspeed.query.service.querybuilder.SummaryQueryBuilder;

import in.zapr.druid.druidry.Interval;
import in.zapr.druid.druidry.client.DruidClient;
import in.zapr.druid.druidry.client.DruidConfiguration;
import in.zapr.druid.druidry.client.DruidJerseyClient;
import in.zapr.druid.druidry.query.aggregation.DruidTimeSeriesQuery;

public class SessionTimeQueryTest {

	public static void main(String[] args) throws Exception {
		final DateTime endTime = DateTime.now();
		final DateTime startTime = DateTime.now().minusDays(30);
		final Interval interval = new Interval(startTime, endTime);
		System.out.println("interval : "+interval.getEndTime() +" "+interval.getStartTime());
		QueryParams params = new QueryParams();
		params.setFromDate(startTime.getMillis());
		params.setToDate(endTime.getMillis());
		final DruidTimeSeriesQuery timeSeriesQuery = SummaryQueryBuilder.getSessionTimeSQL("beacons", params);
		ObjectMapper jsonParser = new ObjectMapper();
		System.out.println(jsonParser.writeValueAsString(timeSeriesQuery));
		final DruidConfiguration config =  DruidConfiguration
	               .builder()
	               .host("ec2-52-66-183-158.ap-south-1.compute.amazonaws.com")
	               .endpoint("druid/v2/")
	               .build();
		final DruidClient client = new DruidJerseyClient(config);
		client.connect();
		final String resp = client.query(timeSeriesQuery);
		System.out.println(resp);
	}
}
