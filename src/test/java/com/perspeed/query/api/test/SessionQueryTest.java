package com.perspeed.query.api.test;

import java.util.Arrays;

import org.joda.time.DateTime;

import com.fasterxml.jackson.databind.ObjectMapper;

import in.zapr.druid.druidry.Interval;
import in.zapr.druid.druidry.aggregator.CountAggregator;
import in.zapr.druid.druidry.aggregator.DruidAggregator;
import in.zapr.druid.druidry.client.DruidClient;
import in.zapr.druid.druidry.client.DruidConfiguration;
import in.zapr.druid.druidry.client.DruidJerseyClient;
import in.zapr.druid.druidry.filter.AndFilter;
import in.zapr.druid.druidry.filter.SelectorFilter;
import in.zapr.druid.druidry.granularity.Granularity;
import in.zapr.druid.druidry.granularity.PredefinedGranularity;
import in.zapr.druid.druidry.granularity.SimpleGranularity;
import in.zapr.druid.druidry.query.aggregation.DruidTimeSeriesQuery;

public class SessionQueryTest {

	public static void main(String[] args) throws Exception {
		final DruidAggregator sessions = new CountAggregator("Completed Sessions");
		final SelectorFilter browserFilter = new SelectorFilter("session_completed", "Y");
		final AndFilter filter = new AndFilter(Arrays.asList(browserFilter));
		final DateTime endTime = DateTime.now();
		final DateTime startTime = DateTime.now().minusDays(30);
		final Interval interval = new Interval(startTime, endTime);
		System.out.println("interval : "+interval.getEndTime() +" "+interval.getStartTime());
		final Granularity granularity = new SimpleGranularity(PredefinedGranularity.ALL);
		final DruidTimeSeriesQuery timeSeriesQuery =  DruidTimeSeriesQuery.builder()
				.dataSource("beacons")
				.granularity(granularity)
				.intervals(Arrays.asList(interval))
				.aggregators(Arrays.asList(sessions))
				.filter(filter)
				.build();
		ObjectMapper jsonParser = new ObjectMapper();
		System.out.println(jsonParser.writeValueAsString(timeSeriesQuery));
		final DruidConfiguration config =  DruidConfiguration
	               .builder()
	               .host("ec2-52-66-183-158.ap-south-1.compute.amazonaws.com")
	               .endpoint("druid/v2/")
	               .build();
		final DruidClient client = new DruidJerseyClient(config);
		client.connect();
		final String resp = client.query(timeSeriesQuery);
		System.out.println(resp);
	}
}
