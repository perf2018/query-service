package com.perfspeed.query.service.resp.druid;

import com.fasterxml.jackson.annotation.JsonProperty;

public class DruidQueryHistoResp {

	@JsonProperty("timestamp")
	private String timeStamp;
	@JsonProperty("result")
	private DruidHistoResult result;
	public String getTimeStamp() {
		return timeStamp;
	}
	public DruidHistoResult getResult() {
		return result;
	}
	public void setTimeStamp(String timeStamp) {
		this.timeStamp = timeStamp;
	}
	public void setResult(DruidHistoResult result) {
		this.result = result;
	}
	@Override
	public String toString() {
		return "DruidQueryHistoResp [timeStamp=" + timeStamp + ", result=" + result + "]";
	}
}
