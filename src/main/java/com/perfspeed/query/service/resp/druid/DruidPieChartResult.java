package com.perfspeed.query.service.resp.druid;

import com.fasterxml.jackson.annotation.JsonProperty;

public class DruidPieChartResult {

	@JsonProperty("DeviceType")
	private String deviceType;
	@JsonProperty("DeviceTypeCount")
	private String deviceTypeCount;
	
	public String getDeviceType() {
		return deviceType;
	}
	public String getDeviceTypeCount() {
		return deviceTypeCount;
	}
	public void setDeviceType(String deviceType) {
		this.deviceType = deviceType;
	}
	public void setDeviceTypeCount(String deviceTypeCount) {
		this.deviceTypeCount = deviceTypeCount;
	}
	@Override
	public String toString() {
		return "DruidPieChartResult [deviceType=" + deviceType + ", deviceTypeCount=" + deviceTypeCount + "]";
	}
}
