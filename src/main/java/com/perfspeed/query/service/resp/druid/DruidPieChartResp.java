package com.perfspeed.query.service.resp.druid;

import java.util.List;

import com.fasterxml.jackson.annotation.JsonProperty;

public class DruidPieChartResp {

	@JsonProperty("timestamp")
	private String timeStamp;
	@JsonProperty("result")
	private List<DruidPieChartResult> result;
	public String getTimeStamp() {
		return timeStamp;
	}
	public List<DruidPieChartResult> getResult() {
		return result;
	}
	public void setTimeStamp(String timeStamp) {
		this.timeStamp = timeStamp;
	}
	public void setResult(List<DruidPieChartResult> result) {
		this.result = result;
	}
	@Override
	public String toString() {
		return "DruidPieChartResp [timeStamp=" + timeStamp + ", result=" + result + "]";
	}
}
