package com.perfspeed.query.service.resp.chart;

import java.util.List;

import com.fasterxml.jackson.annotation.JsonProperty;


public class PerfPieChatResponse {

	@JsonProperty("title")
	private String title;
	@JsonProperty("avatar")
	private String avatar;
	@JsonProperty("type")
	private String type;
	@JsonProperty("data")
	private List<KeyCount> data;
	public String getTitle() {
		return title;
	}
	public String getAvatar() {
		return avatar;
	}
	public String getType() {
		return type;
	}
	public List<KeyCount> getData() {
		return data;
	}
	public void setTitle(String title) {
		this.title = title;
	}
	public void setAvatar(String avatar) {
		this.avatar = avatar;
	}
	public void setType(String type) {
		this.type = type;
	}
	public void setData(List<KeyCount> data) {
		this.data = data;
	}
	@Override
	public String toString() {
		return "PerfPieChatResponse [title=" + title + ", avatar=" + avatar + ", type=" + type + ", data=" + data + "]";
	}
}
