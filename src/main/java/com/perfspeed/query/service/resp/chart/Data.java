package com.perfspeed.query.service.resp.chart;

public class Data {

	private long time;
	private String value;
	
	public long getTime() {
		return time;
	}
	public String getValue() {
		return value;
	}
	public void setTime(long time) {
		this.time = time;
	}
	public void setValue(String value) {
		this.value = value;
	}
	@Override
	public String toString() {
		return "Data [time=" + time + ", value=" + value + "]";
	}
	
}