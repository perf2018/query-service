package com.perfspeed.query.service.resp.chart;

import java.util.List;

import com.fasterxml.jackson.annotation.JsonProperty;

public class Graph {
	
	@JsonProperty("graphName")
	private String graphName;
	@JsonProperty("graphData")
	private List<Data> graphData;
	public String getGraphName() {
		return graphName;
	}
	public List<Data> getGraphData() {
		return graphData;
	}
	public void setGraphName(String graphName) {
		this.graphName = graphName;
	}
	public void setGraphData(List<Data> graphData) {
		this.graphData = graphData;
	}
	@Override
	public String toString() {
		return "Graph [graphName=" + graphName + ", graphData=" + graphData + "]";
	}
}
