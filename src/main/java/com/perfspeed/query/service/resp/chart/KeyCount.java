package com.perfspeed.query.service.resp.chart;

import com.fasterxml.jackson.annotation.JsonProperty;

public class KeyCount {
	@JsonProperty("key")
	private String key;
	@JsonProperty("count")
	private String count;
	public String getKey() {
		return key;
	}
	public String getCount() {
		return count;
	}
	public void setKey(String key) {
		this.key = key;
	}
	public void setCount(String count) {
		this.count = count;
	}
	@Override
	public String toString() {
		return "KeyCount [key=" + key + ", count=" + count + "]";
	}
}
