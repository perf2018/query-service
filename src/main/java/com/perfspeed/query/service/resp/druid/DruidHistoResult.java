package com.perfspeed.query.service.resp.druid;

import com.fasterxml.jackson.annotation.JsonProperty;

public class DruidHistoResult {

	@JsonProperty("PageLoadTime")
	private String pageLoadTime;
	@JsonProperty("PageViews")
	private String pageViews;
	
	public String getPageLoadTime() {
		return pageLoadTime;
	}
	public String getPageViews() {
		return pageViews;
	}
	public void setPageLoadTime(String pageLoadTime) {
		this.pageLoadTime = pageLoadTime;
	}
	public void setPageViews(String pageViews) {
		this.pageViews = pageViews;
	}
	@Override
	public String toString() {
		return "DruidHistoResult [pageLoadTime=" + pageLoadTime + ", pageViews=" + pageViews + "]";
	}
}