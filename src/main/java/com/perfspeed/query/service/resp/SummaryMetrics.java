package com.perfspeed.query.service.resp;

import com.fasterxml.jackson.annotation.JsonProperty;

public class SummaryMetrics {
	
	@JsonProperty("PageLoadTime")
	private String pageLoadTime;
	@JsonProperty("PageViews")
	private String pageViews;
	@JsonProperty("BackEndTime")
	private String backEndTime;
	@JsonProperty("FrontEndTime")
	private String frontEndTime;
	@JsonProperty("DOMLoadTime")
	private String domLoadTime;
	@JsonProperty("DNSTime")
	private String dnsTime;
	@JsonProperty("TCPTime")
	private String tcpTime;
	@JsonProperty("TTFBTime")
	private String ttfbTime;
	@JsonProperty("NWLatencyTime")
	private String nwLatency;
	@JsonProperty("DownLoadTime")
	private String downLoadTime;
	@JsonProperty("SSLTime")
	private String sslTime;
	@JsonProperty("CompletedSessions")
	private String completedSessions;
	@JsonProperty("SessionTime")
	private String sessionTime;
	
	public String getPageLoadTime() {
		return pageLoadTime;
	}
	public String getPageViews() {
		return pageViews;
	}
	public String getBackEndTime() {
		return backEndTime;
	}
	public String getFrontEndTime() {
		return frontEndTime;
	}
	public String getDomLoadTime() {
		return domLoadTime;
	}
	public void setPageLoadTime(String pageLoadTime) {
		this.pageLoadTime = pageLoadTime;
	}
	public void setPageViews(String pageViews) {
		this.pageViews = pageViews;
	}
	public void setBackEndTime(String backEndTime) {
		this.backEndTime = backEndTime;
	}
	public void setFrontEndTime(String frontEndTime) {
		this.frontEndTime = frontEndTime;
	}
	public void setDomLoadTime(String domLoadTime) {
		this.domLoadTime = domLoadTime;
	}
	public String getDnsTime() {
		return dnsTime;
	}
	public String getTcpTime() {
		return tcpTime;
	}
	public String getTtfbTime() {
		return ttfbTime;
	}
	public String getNwLatency() {
		return nwLatency;
	}
	public String getDownLoadTime() {
		return downLoadTime;
	}
	public String getSslTime() {
		return sslTime;
	}
	public void setDnsTime(String dnsTime) {
		this.dnsTime = dnsTime;
	}
	public void setTcpTime(String tcpTime) {
		this.tcpTime = tcpTime;
	}
	public void setTtfbTime(String ttfbTime) {
		this.ttfbTime = ttfbTime;
	}
	public void setNwLatency(String nwLatency) {
		this.nwLatency = nwLatency;
	}
	public void setDownLoadTime(String downLoadTime) {
		this.downLoadTime = downLoadTime;
	}
	public void setSslTime(String sslTime) {
		this.sslTime = sslTime;
	}
	
	public String getCompletedSessions() {
		return completedSessions;
	}
	public void setCompletedSessions(String completedSessions) {
		this.completedSessions = completedSessions;
	}
	public String getSessionTime() {
		return sessionTime;
	}
	public void setSessionTime(String sessionTime) {
		this.sessionTime = sessionTime;
	}
	@Override
	public String toString() {
		return "SummaryMetrics [pageLoadTime=" + pageLoadTime + ", pageViews=" + pageViews + ", backEndTime="
				+ backEndTime + ", frontEndTime=" + frontEndTime + ", domLoadTime=" + domLoadTime + ", dnsTime="
				+ dnsTime + ", tcpTime=" + tcpTime + ", ttfbTime=" + ttfbTime + ", nwLatency=" + nwLatency
				+ ", downLoadTime=" + downLoadTime + ", sslTime=" + sslTime + ", completedSessions=" + completedSessions
				+ ", sessionTime=" + sessionTime + "]";
	}
}