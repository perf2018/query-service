package com.perfspeed.query.service.entity;

public class QueryParams {

	private long toDate;
	private long fromDate;
	private String deviceType;
	private String os;
	
	public long getToDate() {
		return toDate;
	}
	public long getFromDate() {
		return fromDate;
	}
	public String getDeviceType() {
		return deviceType;
	}
	public String getOs() {
		return os;
	}
	public void setToDate(long toDate) {
		this.toDate = toDate;
	}
	public void setFromDate(long fromDate) {
		this.fromDate = fromDate;
	}
	public void setDeviceType(String deviceType) {
		this.deviceType = deviceType;
	}
	public void setOs(String os) {
		this.os = os;
	}
	
	@Override
	public String toString() {
		return "QueryParams [toDate=" + toDate + ", fromDate=" + fromDate + ", deviceType=" + deviceType + ", os=" + os
				+ "]";
	}
}
