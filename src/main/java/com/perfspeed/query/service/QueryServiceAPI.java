package com.perfspeed.query.service;

import javax.annotation.PostConstruct;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.jdbc.DataSourceAutoConfiguration;
import org.springframework.boot.autoconfigure.orm.jpa.HibernateJpaAutoConfiguration;
import org.springframework.boot.builder.SpringApplicationBuilder;
import org.springframework.boot.web.support.SpringBootServletInitializer;
import org.springframework.core.env.Environment;
import org.springframework.scheduling.annotation.EnableAsync;
import org.springframework.scheduling.annotation.EnableScheduling;

import com.perfspeed.query.service.client.DruidQueryClient;



@SpringBootApplication()
@EnableAutoConfiguration(exclude={DataSourceAutoConfiguration.class,HibernateJpaAutoConfiguration.class})
@EnableAsync
@EnableScheduling
public class QueryServiceAPI extends SpringBootServletInitializer {

	private static final Logger LOG = LoggerFactory.getLogger(QueryServiceAPI.class);
	
	@Autowired
	private Environment env;
	
	public static void main(String[] args) {
		SpringApplication.run(QueryServiceAPI.class, args);
	}
	
	@Override
	protected SpringApplicationBuilder configure(SpringApplicationBuilder application) {
		return application.sources(QueryServiceAPI.class);
	}
	
	@PostConstruct
	public void setup() {
		LOG.info("Initializing Query Service Application ");
		DruidQueryClient.INSTANCE.setEnvironment(env);
	}
}
