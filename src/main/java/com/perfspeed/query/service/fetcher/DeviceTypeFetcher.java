package com.perfspeed.query.service.fetcher;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.perfspeed.query.service.client.DruidQueryClient;
import com.perfspeed.query.service.entity.QueryParams;
import com.perfspeed.query.service.querybuilder.DeviceTypeQueryBuilder;
import com.perfspeed.query.service.resp.chart.KeyCount;
import com.perfspeed.query.service.resp.chart.PerfPieChatResponse;
import com.perfspeed.query.service.resp.druid.DruidPieChartResp;
import com.perfspeed.query.service.resp.druid.DruidPieChartResult;
import com.perfspeed.query.service.utils.JsonParser;

import in.zapr.druid.druidry.query.aggregation.DruidTopNQuery;

public class DeviceTypeFetcher {

private static final Logger LOG = LoggerFactory.getLogger(DeviceTypeFetcher.class);
	
	public String dataSource;

	public DeviceTypeFetcher(final String dataSource){
		this.dataSource = dataSource;
	}
	
	public PerfPieChatResponse  fetchDeviceTypeMetrics(final QueryParams query) throws Exception{
		final PerfPieChatResponse pieChartResp = new PerfPieChatResponse();
		final long reqFetchStart = System.currentTimeMillis();
		final DruidTopNQuery topNQuery = DeviceTypeQueryBuilder.getDeviceTypeSQL(dataSource, query);
		System.out.println(JsonParser.objectToJson(topNQuery));
		final String resp = DruidQueryClient.INSTANCE.get().query(topNQuery);
		LOG.info(" Druid Broker Fetched Response for the Request : {} , Response Time : {}  ", query , (System.currentTimeMillis() - reqFetchStart));
		parseResponse(resp, pieChartResp);
		return pieChartResp;
	}
	
	
	public void parseResponse(final String resp , final PerfPieChatResponse pieChartResp){
		pieChartResp.setTitle("Device Classification");
		pieChartResp.setAvatar("P");
		pieChartResp.setType("PieChart");
		final List<DruidPieChartResp> druidResponseList = JsonParser.json2DruidPieChartRespObjList(resp);
		if(Objects.isNull(druidResponseList) || druidResponseList.isEmpty() ) return;
		System.out.println("Size of Response ========> "+druidResponseList.size());
		final List<KeyCount> keyCountMetrics = new ArrayList<KeyCount>();
		for(DruidPieChartResp druidResponse: druidResponseList){
			for(final DruidPieChartResult result : druidResponse.getResult()){
				KeyCount keyCount = new KeyCount();
				keyCount.setCount(result.getDeviceTypeCount());
				keyCount.setKey(result.getDeviceType());
				keyCountMetrics.add(keyCount);
			}
		}
		pieChartResp.setData(keyCountMetrics);
	}
}
