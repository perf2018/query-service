package com.perfspeed.query.service.fetcher;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.perfspeed.query.service.client.DruidQueryClient;
import com.perfspeed.query.service.entity.QueryParams;
import com.perfspeed.query.service.querybuilder.PerfHistoQueryBuilder;
import com.perfspeed.query.service.resp.chart.Data;
import com.perfspeed.query.service.resp.chart.Graph;
import com.perfspeed.query.service.resp.chart.PerfHistoChatResponse;
import com.perfspeed.query.service.resp.druid.DruidQueryHistoResp;
import com.perfspeed.query.service.utils.DateUtils;
import com.perfspeed.query.service.utils.JsonParser;

import in.zapr.druid.druidry.query.aggregation.DruidTimeSeriesQuery;

public class PerfHistoMetircsFetcher {

	private static final Logger LOG = LoggerFactory.getLogger(PerfHistoMetircsFetcher.class);
	
	public String dataSource;

	public PerfHistoMetircsFetcher(final String dataSource){
		this.dataSource = dataSource;
	}
	
	public PerfHistoChatResponse  fetchHistoGramMetrics(final QueryParams query) throws Exception{
		final PerfHistoChatResponse histogramResp = new PerfHistoChatResponse();
		final long reqFetchStart = System.currentTimeMillis();
		final DruidTimeSeriesQuery timeSeriesQuery = PerfHistoQueryBuilder.getPerfTimeSQL(dataSource, query);
		System.out.println(JsonParser.objectToJson(timeSeriesQuery));
		final String resp = DruidQueryClient.INSTANCE.get().query(timeSeriesQuery);
		LOG.info(" Druid Broker Fetched Response for the Request : {} , Response Time : {}  ", query , (System.currentTimeMillis() - reqFetchStart));
		parseResp(histogramResp, resp);
		return histogramResp;
	}
	
	public void parseResp(final PerfHistoChatResponse histogramResp, final String resp){
		histogramResp.setTitle("Performance Over time");
		histogramResp.setAvatar("H");
		histogramResp.setType("Histogram");
		final List<Graph> data = new ArrayList<>();
		
		// GrapList
		final Graph pageLoadGraph = new Graph();
		final Graph pageViewGraph = new Graph();
		pageLoadGraph.setGraphName("Page Load Time(avg)");
		pageViewGraph.setGraphName("Page View(count)");
		final List<DruidQueryHistoResp> druidResponseList = JsonParser.json2ObjList(resp);
		if(Objects.isNull(druidResponseList) || druidResponseList.isEmpty() ) return;
		System.out.println("Size of Response ========> "+druidResponseList.size());
		final List<Data> pageLoadTimeDataList = new ArrayList<>();
		final List<Data> pageViewDataList = new ArrayList<>();
		for(DruidQueryHistoResp druidResponse: druidResponseList){
			final Data pageViewData = new Data();
			final Data pageLoadTimeData = new Data();
			final long time = DateUtils.toDate(druidResponse.getTimeStamp());
			pageLoadTimeData.setTime(time);
			pageLoadTimeData.setValue(druidResponse.getResult().getPageLoadTime());
			pageViewData.setTime(time);
			pageViewData.setValue(druidResponse.getResult().getPageViews());
			pageLoadTimeDataList.add(pageLoadTimeData);
			pageViewDataList.add(pageViewData);
		}
		// add back to Graph
		pageLoadGraph.setGraphData(pageLoadTimeDataList);
		pageViewGraph.setGraphData(pageViewDataList);
		data.add(pageViewGraph);
		data.add(pageLoadGraph);
		histogramResp.setData(data);
	}
}