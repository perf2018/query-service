package com.perfspeed.query.service.fetcher;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.perfspeed.query.service.client.DruidQueryClient;
import com.perfspeed.query.service.entity.QueryParams;
import com.perfspeed.query.service.querybuilder.SummaryQueryBuilder;
import com.perfspeed.query.service.resp.Response;
import com.perfspeed.query.service.resp.SummaryMetrics;
import com.perfspeed.query.service.utils.DruidUtils;
import com.perfspeed.query.service.utils.JsonParser;

import in.zapr.druid.druidry.client.exception.ConnectionException;
import in.zapr.druid.druidry.client.exception.QueryException;
import in.zapr.druid.druidry.query.aggregation.DruidTimeSeriesQuery;

public class SummaryMetricsFetcher {

	private static final Logger LOG = LoggerFactory.getLogger(SummaryMetricsFetcher.class);
	
	public String dataSource;
	
	public SummaryMetricsFetcher(final String dataSource ){
		this.dataSource = dataSource;
	}
	
	/**
	 * Fetch Summary Metrics
	 * @param query
	 * @return
	 * @throws QueryException
	 * @throws ConnectionException
	 */
	public List<Response>  fetchSummary(final QueryParams query) throws Exception{
		final List<Response> result = new ArrayList<Response>();
		fetchTimeMetrics(query, result);
		fetchSessionTimeMetrics(query, result);
		fetchSessionCompletionMetrics(query, result);
		return result;
	}
	
	/**
	 * Fetch Summary Metrics 
	 * @param query 
	 * @return listOfMetrics
	 * @throws QueryException
	 * @throws ConnectionException 
	 */
	public void fetchTimeMetrics(final QueryParams query, final List<Response> result) throws Exception{
		final long reqFetchStart = System.currentTimeMillis();
		final DruidTimeSeriesQuery timeSeriesQuery = SummaryQueryBuilder.getSummarySQL(dataSource, query);
		System.out.println(JsonParser.objectToJson(timeSeriesQuery));
		final String resp = DruidQueryClient.INSTANCE.get().query(timeSeriesQuery);
		LOG.info(" Druid Broker Fetched Response for the Request : {} , Response Time : {}  ", query , (System.currentTimeMillis() - reqFetchStart));
		System.out.println("Resp : "+resp);
		parseSummaryResponse(resp,result);
	}
	
	/**
	 * Fetch number of completed sessions
	 * @param query
	 * @return
	 * @throws QueryException
	 * @throws ConnectionException
	 */
	public void fetchSessionTimeMetrics(final QueryParams query, final List<Response> result) throws Exception{
		final long reqFetchStart = System.currentTimeMillis();
		final DruidTimeSeriesQuery timeSeriesQuery = SummaryQueryBuilder.getSessionTimeSQL(dataSource, query);
		System.out.println(JsonParser.objectToJson(timeSeriesQuery));
		final String resp = DruidQueryClient.INSTANCE.get().query(timeSeriesQuery);
		LOG.info(" Druid Broker Fetched Response for the Request : {} , Response Time : {}  ", query , (System.currentTimeMillis() - reqFetchStart));
		System.out.println("Resp : "+resp);
		if(Objects.isNull(resp) || resp.isEmpty() ) return ;
		final SummaryMetrics metrics = JsonParser.jsonToObject(DruidUtils.extractResultFromResp(resp), SummaryMetrics.class);
		final Response sessionTime = new Response();
		sessionTime.setName("Session Time");
		sessionTime.setValue(metrics.getSessionTime());
		result.add(sessionTime);
	}
	

	/**
	 * Fetch number of completed sessions
	 * @param query
	 * @return
	 * @throws QueryException
	 * @throws ConnectionException
	 */
	public void fetchSessionCompletionMetrics(final QueryParams query, final List<Response> result) throws Exception{
		final long reqFetchStart = System.currentTimeMillis();
		final DruidTimeSeriesQuery timeSeriesQuery = SummaryQueryBuilder.getCompletedSessionSQL(dataSource, query);
		System.out.println(JsonParser.objectToJson(timeSeriesQuery));
		final String resp = DruidQueryClient.INSTANCE.get().query(timeSeriesQuery);
		LOG.info(" Druid Broker Fetched Response for the Request : {} , Response Time : {}  ", query , (System.currentTimeMillis() - reqFetchStart));
		System.out.println("Resp : "+resp);
		if(Objects.isNull(resp) || resp.isEmpty() ) return ;
		final SummaryMetrics metrics = JsonParser.jsonToObject(DruidUtils.extractResultFromResp(resp), SummaryMetrics.class);
		final Response sessionTotal = new Response();
		sessionTotal.setName("NoOfCompletedSessions");
		sessionTotal.setValue(metrics.getCompletedSessions());
		result.add(sessionTotal);
	}
	
	/**
	 * 
	 * @param summaryList
	 * @return
	 */
	public void parseSummaryResponse(final String resp, final List<Response> result){
		if(Objects.isNull(resp) || resp.isEmpty() ) return ;
		final SummaryMetrics metrics = JsonParser.jsonToObject(DruidUtils.extractResultFromResp(resp), SummaryMetrics.class);
		final Response pageViewResp = new Response();
		pageViewResp.setName("PageViews");
		pageViewResp.setValue(metrics.getPageViews());
		final Response pageLoadTimeResp = new Response();
		pageLoadTimeResp.setName("PageLoadTime");
		pageLoadTimeResp.setValue(metrics.getPageLoadTime());
		final Response backEndResp = new Response();
		backEndResp.setName("BackEndTime");
		backEndResp.setValue(metrics.getBackEndTime());
		final Response frontEndTimeResp = new Response();
		frontEndTimeResp.setName("FrontEndTime");
		frontEndTimeResp.setValue(metrics.getFrontEndTime());
		final Response domLoadedTime = new Response();
		domLoadedTime.setName("DOMLoadTime");
		domLoadedTime.setValue(metrics.getDomLoadTime());
		final Response dnsTime = new Response();
		dnsTime.setName("DNSTime");
		dnsTime.setValue(metrics.getDnsTime());
		final Response tcpTime = new Response();
		tcpTime.setName("TCPTime");
		tcpTime.setValue(metrics.getTcpTime());
		final Response ttfbTime = new Response();
		ttfbTime.setName("TTFBTime");
		ttfbTime.setValue(metrics.getTtfbTime());
		final Response nwLatencyTime = new Response();
		nwLatencyTime.setName("NWLatencyTime");
		nwLatencyTime.setValue(metrics.getNwLatency());
		final Response downLoadTime = new Response();
		downLoadTime.setName("DownLoadTime");
		downLoadTime.setValue(metrics.getDownLoadTime());
		final Response sslTime = new Response();
		sslTime.setName("SSLTime");
		sslTime.setValue(metrics.getSslTime());
		result.add(pageViewResp);
		result.add(pageLoadTimeResp);
		result.add(backEndResp);
		result.add(frontEndTimeResp);
		result.add(domLoadedTime);
		result.add(dnsTime);
		result.add(tcpTime);
		result.add(ttfbTime);
		result.add(nwLatencyTime);
		result.add(downLoadTime);
		result.add(sslTime);
	}
}
