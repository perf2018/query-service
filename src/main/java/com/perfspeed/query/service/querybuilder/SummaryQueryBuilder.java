package com.perfspeed.query.service.querybuilder;

import java.util.Arrays;

import org.joda.time.DateTime;

import com.perfspeed.query.service.entity.QueryParams;

import in.zapr.druid.druidry.Interval;
import in.zapr.druid.druidry.aggregator.CountAggregator;
import in.zapr.druid.druidry.aggregator.DruidAggregator;
import in.zapr.druid.druidry.aggregator.LongSumAggregator;
import in.zapr.druid.druidry.filter.AndFilter;
import in.zapr.druid.druidry.filter.SelectorFilter;
import in.zapr.druid.druidry.granularity.Granularity;
import in.zapr.druid.druidry.granularity.PredefinedGranularity;
import in.zapr.druid.druidry.granularity.SimpleGranularity;
import in.zapr.druid.druidry.postAggregator.ArithmeticFunction;
import in.zapr.druid.druidry.postAggregator.ArithmeticOrdering;
import in.zapr.druid.druidry.postAggregator.ArithmeticPostAggregator;
import in.zapr.druid.druidry.postAggregator.DruidPostAggregator;
import in.zapr.druid.druidry.postAggregator.FieldAccessPostAggregator;
import in.zapr.druid.druidry.query.aggregation.DruidTimeSeriesQuery;

public class SummaryQueryBuilder {

	public static DruidTimeSeriesQuery getSummarySQL(final String dataSource, final QueryParams query){
		try{
			final DruidAggregator pageViews = new CountAggregator("PageViews");
			final DruidAggregator pageLoadSum = new LongSumAggregator("pageload_time:sum","pageload_time");
			final DruidAggregator pageLoadCount = new CountAggregator("pageload_time:count");
			final DruidPostAggregator pageLoadTimeSum = new FieldAccessPostAggregator("pageload_time:sum");
			final DruidPostAggregator pageLoadTimeCount  = new FieldAccessPostAggregator("pageload_time:count");
			final DruidAggregator backEndSum = new LongSumAggregator("backend_time:sum","backend_time");
			final DruidAggregator backEndCount = new CountAggregator("backend_time:count");
			final DruidPostAggregator backEndTimeSumField = new FieldAccessPostAggregator("backend_time:sum");
			final DruidPostAggregator backEndTimeCountField  = new FieldAccessPostAggregator("backend_time:count");
			final DruidAggregator frontEndTimeSum = new LongSumAggregator("frontend_time:sum","frontend_time");
			final DruidAggregator frontEndTimeCount = new CountAggregator("frontend_time:count");
			final DruidPostAggregator frontEndTimeSumField = new FieldAccessPostAggregator("frontend_time:sum");
			final DruidPostAggregator frontEndTimeCountField  = new FieldAccessPostAggregator("frontend_time:count");
			final DruidAggregator dnsSum = new LongSumAggregator("dns:sum","dns");
			final DruidAggregator dnsCount = new CountAggregator("dns:count");
			final DruidPostAggregator dnsSumField = new FieldAccessPostAggregator("dns:sum");
			final DruidPostAggregator dnsCountField  = new FieldAccessPostAggregator("dns:count");
			final DruidAggregator tcpSum = new LongSumAggregator("tcp:sum","tcp");
			final DruidAggregator tcpCount = new CountAggregator("tcp:count");
			final DruidPostAggregator tcpSumField = new FieldAccessPostAggregator("tcp:sum");
			final DruidPostAggregator tcpCountField  = new FieldAccessPostAggregator("tcp:count");
			final DruidAggregator domSum = new LongSumAggregator("dom:sum","dom");
			final DruidAggregator domCount = new CountAggregator("dom:count");
			final DruidPostAggregator domSumField = new FieldAccessPostAggregator("dom:sum");
			final DruidPostAggregator domCountField  = new FieldAccessPostAggregator("dom:count");
			final DruidAggregator ttfbSum = new LongSumAggregator("ttfb:sum","ttfb");
			final DruidAggregator ttfbCount = new CountAggregator("ttfb:count");
			final DruidPostAggregator ttfbSumField = new FieldAccessPostAggregator("ttfb:sum");
			final DruidPostAggregator ttfbCountField  = new FieldAccessPostAggregator("ttfb:count");
			final DruidAggregator latencySum = new LongSumAggregator("latency:sum","latency");
			final DruidAggregator latencyCount = new CountAggregator("latency:count");
			final DruidPostAggregator latencySumField = new FieldAccessPostAggregator("latency:sum");
			final DruidPostAggregator latencyCountField  = new FieldAccessPostAggregator("latency:count");
			final DruidAggregator downloadSum = new LongSumAggregator("download:sum","download");
			final DruidAggregator downloadCount = new CountAggregator("download:count");
			final DruidPostAggregator downloadSumField = new FieldAccessPostAggregator("download:sum");
			final DruidPostAggregator downloadCountField  = new FieldAccessPostAggregator("download:count");
			final DruidAggregator sslSum = new LongSumAggregator("ssl:sum","ssl");
			final DruidAggregator sslCount = new CountAggregator("ssl:count");
			final DruidPostAggregator sslSumField = new FieldAccessPostAggregator("ssl:sum");
			final DruidPostAggregator sslCountField  = new FieldAccessPostAggregator("ssl:count");
			final Granularity granularity = new SimpleGranularity(PredefinedGranularity.ALL);
			final DateTime endTime = new DateTime(query.getToDate());
			final DateTime startTime = new DateTime(query.getFromDate());
			final Interval interval = new Interval(startTime, endTime);
			final DruidPostAggregator pageLoadAggr = ArithmeticPostAggregator.builder()
					 .name("PageLoadTime")
					 .function(ArithmeticFunction.QUOTIENT)
					 .fields(Arrays.asList(pageLoadTimeSum, pageLoadTimeCount))
					 .ordering(ArithmeticOrdering.NUMERIC_FIRST)
					 .build();
			final DruidPostAggregator backEndAggr = ArithmeticPostAggregator.builder()
					 .name("BackEndTime")
					 .function(ArithmeticFunction.QUOTIENT)
					 .fields(Arrays.asList(backEndTimeSumField, backEndTimeCountField))
					 .ordering(ArithmeticOrdering.NUMERIC_FIRST)
					 .build();
			final DruidPostAggregator frondEndTimeAggr = ArithmeticPostAggregator.builder()
					 .name("FrontEndTime")
					 .function(ArithmeticFunction.QUOTIENT)
					 .fields(Arrays.asList(frontEndTimeSumField, frontEndTimeCountField))
					 .ordering(ArithmeticOrdering.NUMERIC_FIRST)
					 .build();
			final DruidPostAggregator domLoadedTimeAggr = ArithmeticPostAggregator.builder()
					 .name("DOMLoadTime")
					 .function(ArithmeticFunction.QUOTIENT)
					 .fields(Arrays.asList(domSumField, domCountField))
					 .ordering(ArithmeticOrdering.NUMERIC_FIRST)
					 .build();
			final DruidPostAggregator dnsTimeAggr = ArithmeticPostAggregator.builder()
					 .name("DNSTime")
					 .function(ArithmeticFunction.QUOTIENT)
					 .fields(Arrays.asList(dnsSumField, dnsCountField))
					 .ordering(ArithmeticOrdering.NUMERIC_FIRST)
					 .build();
			final DruidPostAggregator tcpTimeAggr = ArithmeticPostAggregator.builder()
					 .name("TCPTime")
					 .function(ArithmeticFunction.QUOTIENT)
					 .fields(Arrays.asList(tcpSumField, tcpCountField))
					 .ordering(ArithmeticOrdering.NUMERIC_FIRST)
					 .build();
			final DruidPostAggregator ttfbTimeAggr = ArithmeticPostAggregator.builder()
					 .name("TTFBTime")
					 .function(ArithmeticFunction.QUOTIENT)
					 .fields(Arrays.asList(ttfbSumField, ttfbCountField))
					 .ordering(ArithmeticOrdering.NUMERIC_FIRST)
					 .build();
			final DruidPostAggregator latencyTimeAggr = ArithmeticPostAggregator.builder()
					 .name("NWLatencyTime")
					 .function(ArithmeticFunction.QUOTIENT)
					 .fields(Arrays.asList(latencySumField, latencyCountField))
					 .ordering(ArithmeticOrdering.NUMERIC_FIRST)
					 .build();
			final DruidPostAggregator downLoadTimeAggr = ArithmeticPostAggregator.builder()
					 .name("DownLoadTime")
					 .function(ArithmeticFunction.QUOTIENT)
					 .fields(Arrays.asList(downloadSumField, downloadCountField))
					 .ordering(ArithmeticOrdering.NUMERIC_FIRST)
					 .build();
			final DruidPostAggregator sslTimeAggr = ArithmeticPostAggregator.builder()
					 .name("SSLTime")
					 .function(ArithmeticFunction.QUOTIENT)
					 .fields(Arrays.asList(sslSumField, sslCountField))
					 .ordering(ArithmeticOrdering.NUMERIC_FIRST)
					 .build();
			final DruidTimeSeriesQuery timeSeriesQuery =  DruidTimeSeriesQuery.builder()
					.dataSource(dataSource)
					.granularity(granularity)
					.intervals(Arrays.asList(interval))
					.aggregators(Arrays.asList(pageViews,pageLoadSum,pageLoadCount,
												backEndSum,backEndCount, frontEndTimeSum,frontEndTimeCount, 
											    dnsSum, dnsCount, tcpSum, tcpCount, domSum, domCount, 
											    ttfbSum, ttfbCount , latencySum, latencyCount, 
											    downloadSum, downloadCount, sslSum, sslCount ))
					.postAggregators(Arrays.asList(pageLoadAggr,backEndAggr,frondEndTimeAggr,
													domLoadedTimeAggr, dnsTimeAggr, tcpTimeAggr, 
												    ttfbTimeAggr, latencyTimeAggr, downLoadTimeAggr, sslTimeAggr))
					.build();
			return timeSeriesQuery;
		}catch (Exception e) {
			e.printStackTrace();
		}
		return null;
	}
	
	public static DruidTimeSeriesQuery getCompletedSessionSQL(final String dataSource, final QueryParams query){
		try{
			final DruidAggregator sessions = new CountAggregator("CompletedSessions");
			final SelectorFilter browserFilter = new SelectorFilter("session_completed", "Y");
			final AndFilter filter = new AndFilter(Arrays.asList(browserFilter));
			final Granularity granularity = new SimpleGranularity(PredefinedGranularity.ALL);
			final DateTime endTime = new DateTime(query.getToDate());
			final DateTime startTime = new DateTime(query.getFromDate());
			final Interval interval = new Interval(startTime, endTime);
			final DruidTimeSeriesQuery timeSeriesQuery =  DruidTimeSeriesQuery.builder()
					.dataSource(dataSource)
					.granularity(granularity)
					.intervals(Arrays.asList(interval))
					.aggregators(Arrays.asList(sessions))
					.filter(filter)
					.build();
			return timeSeriesQuery;
		}catch (Exception e) {
			e.printStackTrace();
		}
		return null;
	}
	
	// ToDo:
	// if this session avg time is only for completed sessions - merge count and time query
	public static DruidTimeSeriesQuery getSessionTimeSQL(final String dataSource, final QueryParams query){
		try{
			final SelectorFilter sessionFilter = new SelectorFilter("is_session_data", "Y");
			final AndFilter filter = new AndFilter(Arrays.asList(sessionFilter));
			final DruidAggregator sessionTime = new LongSumAggregator("session_in_focus:sum","session_in_focus");
			final DruidAggregator sessionTimeCount = new CountAggregator("session_in_focus:count");
			final DruidPostAggregator sessionTimeSumField = new FieldAccessPostAggregator("session_in_focus:sum");
			final DruidPostAggregator sessionTimeCountField  = new FieldAccessPostAggregator("session_in_focus:count");
			final DruidPostAggregator sessionTimePostAgg = ArithmeticPostAggregator.builder()
					 .name("SessionTime")
					 .function(ArithmeticFunction.QUOTIENT)
					 .fields(Arrays.asList(sessionTimeSumField, sessionTimeCountField))
					 .ordering(ArithmeticOrdering.NUMERIC_FIRST)
					 .build();
			final Granularity granularity = new SimpleGranularity(PredefinedGranularity.ALL);
			final DateTime endTime = new DateTime(query.getToDate());
			final DateTime startTime = new DateTime(query.getFromDate());
			final Interval interval = new Interval(startTime, endTime);
			final DruidTimeSeriesQuery timeSeriesQuery =  DruidTimeSeriesQuery.builder()
					.dataSource(dataSource)
					.granularity(granularity)
					.intervals(Arrays.asList(interval))
					.aggregators(Arrays.asList(sessionTime,sessionTimeCount))
					.postAggregators(Arrays.asList(sessionTimePostAgg))
					.filter(filter)
					.build();
			return timeSeriesQuery;
		}catch (Exception e) {
			e.printStackTrace();
		}
		return null;
	}
}
