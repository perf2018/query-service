package com.perfspeed.query.service.querybuilder;

import java.util.Arrays;

import org.joda.time.DateTime;

import com.perfspeed.query.service.entity.QueryParams;

import in.zapr.druid.druidry.Interval;
import in.zapr.druid.druidry.aggregator.CountAggregator;
import in.zapr.druid.druidry.aggregator.DruidAggregator;
import in.zapr.druid.druidry.dimension.DefaultDimension;
import in.zapr.druid.druidry.dimension.DruidDimension;
import in.zapr.druid.druidry.dimension.enums.OutputType;
import in.zapr.druid.druidry.granularity.Granularity;
import in.zapr.druid.druidry.granularity.PredefinedGranularity;
import in.zapr.druid.druidry.granularity.SimpleGranularity;
import in.zapr.druid.druidry.query.aggregation.DruidTopNQuery;
import in.zapr.druid.druidry.topNMetric.SimpleMetric;
import in.zapr.druid.druidry.topNMetric.TopNMetric;

public class DeviceTypeQueryBuilder {

	public static DruidTopNQuery getDeviceTypeSQL(final String dataSource, final QueryParams query){
		try{
			final DruidAggregator deviceTypeCount = new CountAggregator("DeviceTypeCount");
			final DruidDimension dimension = new DefaultDimension("deviceType", "DeviceType", OutputType.STRING);
			final Granularity granularity = new SimpleGranularity(PredefinedGranularity.ALL);
			final DateTime endTime = new DateTime(query.getToDate());
			final DateTime startTime = new DateTime(query.getFromDate());
			final Interval interval = new Interval(startTime, endTime);
			final TopNMetric metric = new SimpleMetric("DeviceTypeCount");
			final DruidTopNQuery topNQuery = DruidTopNQuery.builder()
											 .aggregators(Arrays.asList(deviceTypeCount))
											 .dataSource(dataSource)
											 .dimension(dimension)
											 .granularity(granularity)
											 .intervals(Arrays.asList(interval))
											 .topNMetric(metric)
											 .threshold(5000)
											 .build();
			return topNQuery;
		}catch (Exception e) {
			e.printStackTrace();
		}
		return null;
	}
}
