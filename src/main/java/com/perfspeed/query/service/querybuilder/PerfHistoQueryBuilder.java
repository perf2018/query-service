package com.perfspeed.query.service.querybuilder;

import java.util.Arrays;

import org.joda.time.DateTime;

import com.perfspeed.query.service.entity.QueryParams;

import in.zapr.druid.druidry.Interval;
import in.zapr.druid.druidry.aggregator.CountAggregator;
import in.zapr.druid.druidry.aggregator.DruidAggregator;
import in.zapr.druid.druidry.aggregator.LongSumAggregator;
import in.zapr.druid.druidry.granularity.Granularity;
import in.zapr.druid.druidry.granularity.PredefinedGranularity;
import in.zapr.druid.druidry.granularity.SimpleGranularity;
import in.zapr.druid.druidry.postAggregator.ArithmeticFunction;
import in.zapr.druid.druidry.postAggregator.ArithmeticOrdering;
import in.zapr.druid.druidry.postAggregator.ArithmeticPostAggregator;
import in.zapr.druid.druidry.postAggregator.DruidPostAggregator;
import in.zapr.druid.druidry.postAggregator.FieldAccessPostAggregator;
import in.zapr.druid.druidry.query.aggregation.DruidTimeSeriesQuery;

public class PerfHistoQueryBuilder {

	public static DruidTimeSeriesQuery getPerfTimeSQL(final String dataSource, final QueryParams query){
		try{
			final DruidAggregator pageViews = new CountAggregator("PageViews");
			final DruidAggregator pageLoadSum = new LongSumAggregator("pageload_time:sum","pageload_time");
			final DruidAggregator pageLoadCount = new CountAggregator("pageload_time:count");
			final DruidPostAggregator pageLoadTimeSum = new FieldAccessPostAggregator("pageload_time:sum");
			final DruidPostAggregator pageLoadTimeCount  = new FieldAccessPostAggregator("pageload_time:count");
			final DruidPostAggregator pageLoadAggr = ArithmeticPostAggregator.builder()
					 .name("PageLoadTime")
					 .function(ArithmeticFunction.QUOTIENT)
					 .fields(Arrays.asList(pageLoadTimeSum, pageLoadTimeCount))
					 .ordering(ArithmeticOrdering.NUMERIC_FIRST)
					 .build();
			final Granularity granularity = new SimpleGranularity(PredefinedGranularity.MINUTE);
			final DateTime endTime = new DateTime(query.getToDate());
			final DateTime startTime = new DateTime(query.getFromDate());
			final Interval interval = new Interval(startTime, endTime);
			final DruidTimeSeriesQuery timeSeriesQuery =  DruidTimeSeriesQuery.builder()
					.dataSource(dataSource)
					.granularity(granularity)
					.intervals(Arrays.asList(interval))
					.aggregators(Arrays.asList(pageViews,pageLoadSum,pageLoadCount))
					.postAggregators(Arrays.asList(pageLoadAggr))
					.build();
			return timeSeriesQuery;
		}catch (Exception e) {
			e.printStackTrace();
		}
		return null;
	}
}
