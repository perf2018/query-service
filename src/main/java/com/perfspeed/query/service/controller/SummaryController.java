package com.perfspeed.query.service.controller;

import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.env.Environment;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.perfspeed.query.service.entity.QueryParams;
import com.perfspeed.query.service.fetcher.SummaryMetricsFetcher;
import com.perfspeed.query.service.resp.Response;


@RestController
@RequestMapping("/summary")
public class SummaryController {
	
	private static final Logger LOG = LoggerFactory.getLogger(SummaryController.class);
	
	@Autowired
	private Environment env;
	
	@PostMapping("/")
	public List<Response> getSummaryMetrics(final HttpServletRequest request,
				@RequestBody final QueryParams query) throws Exception {
		LOG.info("Query Params : {} ", query);
		SummaryMetricsFetcher summaryMetricsFetcher = new SummaryMetricsFetcher(env.getProperty("druid.io.datasource"));
		return summaryMetricsFetcher.fetchSummary(query);
	}
	
}
