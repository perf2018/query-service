package com.perfspeed.query.service.utils;


import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.perfspeed.query.service.resp.druid.DruidPieChartResp;
import com.perfspeed.query.service.resp.druid.DruidQueryHistoResp;


/**
 * Jackson JSON Parser API that allows to convert json str to obj vice versa
 * 
 * @author skalaise
 *
 */
public class JsonParser {
	private final static Logger LOG = LoggerFactory.getLogger(JsonParser.class);
	private final static ObjectMapper jsonParser = new ObjectMapper();

	/**
	 * Configure JSON Parser in static way
	 */
	static {
		jsonParser.configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false);
		jsonParser.setSerializationInclusion(JsonInclude.Include.NON_NULL);
		jsonParser.configure(DeserializationFeature.FAIL_ON_IGNORED_PROPERTIES, false);
	}

	/**
	 * Convert Object to JSON String 
	 * @param obj which object to convert
	 * @return json string
	 */
	public static <T> String objectToJson(T obj) {
		String jsonString = "";
		try {
			jsonString = jsonParser.writeValueAsString(obj);
		} catch (Exception e) {
			LOG.error(" Cannot convert object to json str ", e);
		}
		return jsonString;
	}
	
	/**
	 * Convert JSON String to Object
	 * @param jsonString to be converted String
	 * @param clazz type of Object
	 * @return object
	 */
	public static <T> T jsonToObject(String jsonString, Class<T> clazz) {
		T obj = null;
		try {
			obj = jsonParser.readValue(jsonString, clazz);
		} catch (Exception e) {
			LOG.error(" Cannot parse json to obj ", e);
		}
		return obj;
	}
	

	public static List<DruidQueryHistoResp> json2ObjList(final String resp){
		List<DruidQueryHistoResp> histoResp = null;
		try{
			histoResp =jsonParser.readValue(resp, new TypeReference<List<DruidQueryHistoResp>>(){});
		}catch (Exception e) {
			LOG.error(" Cannot parse json to list of objects ", e);
		}
		return histoResp;
	}
	
	public static List<DruidPieChartResp> json2DruidPieChartRespObjList(final String resp){
		List<DruidPieChartResp> histoResp = null;
		try{
			histoResp =jsonParser.readValue(resp, new TypeReference<List<DruidPieChartResp>>(){});
		}catch (Exception e) {
			LOG.error(" Cannot parse json to list of objects ", e);
		}
		return histoResp;
	}
	
	/**
	 * Convert Map to Object 
	 * @param map key value pairs
	 * @param clazz Class Which needs to be Converted
	 * @return Object
	 */
	public static <T> Object map2Obj( Map<String,String> map , Class<T> clazz ){
		return jsonParser.convertValue(map, clazz);
	}
	
	public static <T> Object obj2Map( Object fromValue , Class<T> clazz ){
		return jsonParser.convertValue(fromValue, clazz);
	}
	
	@SuppressWarnings("unchecked")
	public static Map<String,String> objToMap( Object fromValue ){
		return jsonParser.convertValue(fromValue, HashMap.class);
	}
}

