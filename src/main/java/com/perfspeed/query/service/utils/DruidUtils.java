package com.perfspeed.query.service.utils;

public class DruidUtils {
	
	public static final String RESULT_REG_EXP = "\"result\":";

	public static String extractResultFromResp(final String resp){
		final StringBuffer buff = new StringBuffer(resp);
		// think about below - revist and ensure to rewrite with other API . Right now brain has some problem to think ha ha ha...
		buff.deleteCharAt(0);
		if(buff.length() > 1)
			buff.deleteCharAt(buff.length()-1);
		if(buff.length() > 1)
			buff.deleteCharAt(buff.length()-1);
		final int indexOfResult = buff.lastIndexOf(RESULT_REG_EXP);
		if(indexOfResult < 0 ) return resp;
		return buff.substring(indexOfResult).replaceAll(RESULT_REG_EXP, "");
	}
}
