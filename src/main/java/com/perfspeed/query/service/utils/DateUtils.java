package com.perfspeed.query.service.utils;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.TimeZone;

public class DateUtils {
	
	private static final DateFormat _dateParser =  new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSS");

	static{
		_dateParser.setTimeZone(TimeZone.getTimeZone("UTC"));
	}
	
	// this assumes client pass UTC epoch time with milli seconds
	// works for time with ms & without ms 
	public static Date convertEpoch2UTCDate( String epochString ) {
		try{
			final Double dateTimeWithMsPrecision = (Double.parseDouble(epochString) * 1000);
			final long timeInMills = ( dateTimeWithMsPrecision.longValue() ) ; // epoch to ms
			Date accessTime = new Date(timeInMills);
			return accessTime;
		}catch (Exception e) {
		}
		// make it to now 
		return new Date();
	}
	
	public static String formatUTCDate( Date utcDate ) {
		String dateTime = null;
		try{
			dateTime = _dateParser.format(utcDate);
		}catch (Exception e) {
		}
		// expected : JVM should not call below code
		// make sure we send Now date if calls
		Date dt = new Date();
		dateTime = _dateParser.format(dt);
		return dateTime;
	}
	
	public static String formatUTCDate( String epochTime ) {
		return formatUTCDate(convertEpoch2UTCDate(epochTime));
	}
	

	public static Long getCurrentUTCTime(){
		Calendar cal = Calendar.getInstance(TimeZone.getTimeZone("GMT"));
		return cal.getTimeInMillis();
	}
	
	public static String getCurrentUTCTimeStr(){
		Calendar cal = Calendar.getInstance(TimeZone.getTimeZone("GMT"));
		return String.valueOf(cal.getTimeInMillis());
	}
	
	
	public static String formatTime(long time ){
		_dateParser.setTimeZone(TimeZone.getTimeZone("UTC"));
        return _dateParser.format(time);
	}
	/**
	 * Convert dateTime to Date Object
	 * @param dateTime
	 * @return dateInUtc
	 */
	public static long toDate(final String dateTime ){
		Date dt = new Date();
		try{
			dt = _dateParser.parse(dateTime);
		}catch (Exception e) {
			e.printStackTrace();
		}
		return dt.getTime();
	}
	
	public static void main(String[] args) {
		final String dateTime = "2018-08-24T17:53:00.000Z";
		System.out.println("2018-08-24T17:53:00.000Z");
		System.out.println(toDate(dateTime));
	}
	
}
