package com.perfspeed.query.service.client;

import org.springframework.core.env.Environment;

import in.zapr.druid.druidry.client.DruidClient;
import in.zapr.druid.druidry.client.DruidConfiguration;
import in.zapr.druid.druidry.client.DruidConfiguration.DruidConfigurationBuilder;
import in.zapr.druid.druidry.client.DruidJerseyClient;
import in.zapr.druid.druidry.client.exception.ConnectionException;

public enum DruidQueryClient {
	
	INSTANCE;
	
	private Environment env;
	
	private final DruidConfigurationBuilder druidConfig =  DruidConfiguration.builder();
	private  DruidClient _client; 
	public  DruidClient  get() throws ConnectionException{
		if(null == _client  || !isDruidClientActive()){
			druidConfig.endpoint(env.getProperty("druid.io.endpoint"));
			druidConfig.host(env.getProperty("druid.io.host"));
			_client =  new DruidJerseyClient(druidConfig.build());
		}
		_client.connect();
		return _client;
	}
	
	public void setEnvironment(final Environment env){
		this.env = env;
	}
	
	public boolean isDruidClientActive(){
		try{
			_client.connect();
			return true;
		}catch (Exception e) {
			e.printStackTrace();
		}
		return false;
	}
}
